import React from 'react';
import './Header.css';
import {Nav, Navbar} from "react-bootstrap";
import {Link} from "react-router-dom";

const Header = () => {
    return (
        <div className="Header">
            <Navbar inverse>
                <Navbar.Header>
                    <Navbar.Brand >
                        <a href="/" className="Home">Home</a>
                    </Navbar.Brand>
                    <Navbar.Toggle />
                </Navbar.Header>i
                <Navbar.Collapse>
                    <Nav pullRight >
                        <Link to="/pages/contacts" className="Header-buttons" >Contacts</Link>
                        <Link to="/pages/about" className="Header-buttons" >About</Link>
                        <Link to="/pages/relatedLinks" className="Header-buttons" >Related Links</Link>
                        <Link to="/pages/admin" className="Header-buttons" >Admin</Link>
                    </Nav>
                </Navbar.Collapse>
            </Navbar>;
        </div>
    )
}

export default Header