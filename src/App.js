import React, { Component,Fragment } from 'react';
import './App.css';
import {Route} from "react-router";
import Header from "./components/Header/Header";
import Page from "./containers/MainPage/MainPage";

class App extends Component {
  render() {
    return (
        <Fragment>
          <Header />
          <Route path='/pages/:content' exact component={Page}/>
        </Fragment>
    );
  }
}

export default App;
