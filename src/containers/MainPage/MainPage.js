import React from 'react';
import axios from 'axios';

class Page extends React.Component {
    state = {
        page: {}
    }

    getData = (data) => {
        axios.get(`/${data}.json`).then(response => {
            if(response.data !== null) {
                this.setState({page: response.data})
            }
        }
        )
    };

    componentDidMount () {
        this.getData(this.props.match.params.content)
    }

    componentDidUpdate (prevProps) {
        if (prevProps.match.params.content !== this.props.match.params.content ) {
            this.getData(this.props.match.params.content)
        }
    }

    render () {

        return (
            <div className="container">
                <h3>{this.state.page.title}</h3>
                <p>{this.state.page.content}</p>
            </div>
        )
    }
}

export default Page;